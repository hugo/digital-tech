#!/usr/bin/guile \
-s
!#

(use-modules (srfi srfi-1)
             (ice-9 match)
             (ice-9 curried-definitions))

(define (xor a b)
  (or (and a (not b))
      (and b (not a))))

(define (cross-product a b)
  (concatenate
   (map (lambda (ai)
          (map (lambda (bi) (list ai bi)) b))
        a)))

(define* (integer->bitvector int #:key len)
  (let loop ((n int) (lst '()))
    (if (zero? n)
        (list->bitvector
         (if len
             (append (make-list (- len (length lst)) #f) lst)
             lst))
        (call-with-values (lambda ()  (floor/ n 2))
          (lambda (rest bit)
            (loop rest (cons (= 1 bit) lst)))))))



(define (bitvector->integer bv)
  (apply +
   (map cadr
        (filter car
                (zip (reverse (bitvector->list bv))
                     (map (lambda (x) (expt 2 x)) (iota (bitvector-length bv))))))))


;; (define inputs (map (lambda (n) (integer->bitvector n #:len 3))
;;                     (iota (expt 2 3))))



#|
d2 = c1 * c2 * b1 = 0
d1 = c2 ⊻ (c1 * b1)
d0 = c1 ⊻ b1


|#

;; (zip
;;  inputs
;;  (map (lambda (bv) (list->bitvector (apply adder (bitvector->list bv))))
;;       inputs
;;       ))


(define (cross-square x) (cross-product x x))

(define ((i->bv len) i)
  (integer->bitvector i #:len len))

(define (2s n) (iota (expt 2 n)))

(define (half-adder a b)
  (values (and a b)
          (xor a b)))

(define (adder a b c)
  (define-values (c0 s0) (half-adder a b))
  (define-values (c1 s1) (half-adder s0 c))
  (values (or c1 c0) s1))

(define (1-bit-adder a b)
  (define-values (c s) (adder a b #f))
  (bitvector c s))

(define (2-bit-adder a b)
  (define-values (c0 s0) (adder (bitvector-ref a 1)
                                (bitvector-ref b 1)
                                #f))
  (define-values (c1 s1) (adder (bitvector-ref a 0)
                                (bitvector-ref b 0)
                                c0))
  (bitvector c1 s1 s0))

(define (left-pad lst target-length)
  (append (make-list (- target-length (length lst)) #f)
          lst))

(define (n-bit-adder a b)
  (define ml (max (bitvector-length a)
                  (bitvector-length b)))
  (list->bitvector
   (fold (lambda (as bs done)
           (call-with-values (lambda () (adder as bs (car done)))
             (lambda (c s) (cons* c s (cdr done)))))
         '(#f)
         (reverse (left-pad (bitvector->list a) ml))
         (reverse (left-pad (bitvector->list b) ml)))))


;; (define inputs
;;  (let ((l (map (lambda (n) (integer->bitvector n #:len 2)) (iota (expt 2 2)))))
;;    (cross-product l l)))


;; (define result
;;  (zip inputs
;;       (map (lambda (pair)
;;              (match (map bitvector->list pair)
;;                (((b1 b0) (a1 a0))
;;                 (match (adder b1 b0 a0)
;;                   ((c2 c1 c0)
;;                    (match (adder c2 c1 a1)
;;                      ((d2 d1 d0)
;;                       (bitvector d2 d1 d0 c0))))))))
;;            inputs)))


(define (add a b)
  (bitvector->integer (n-bit-adder (integer->bitvector a)
                                   (integer->bitvector b))))


(display (add 1234 1234))
(newline)

;; => ((#*00 #*0) (#*00 #*1) (#*01 #*0) (#*01 #*1) (#*10 #*0) (#*10 #*1) (#*11 #*0) (#*11 #*1))

