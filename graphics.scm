(load "dig.scm")

(use-modules (srfi srfi-1)
             (srfi srfi-26)
             (pict)
             (rnrs records syntactic)
             )

(define gate-width 50)
(define gate-height 70)
(define gate-box-width 100)
(define gate-box-height 100)

(define-record-type vec
  (fields x y))

(define v make-vec)

((@ (srfi srfi-9 gnu) set-record-type-printer!)
 vec (lambda (v p) (format p "#<vec ~a ~a>" (vec-x v) (vec-y v))))

(define (v+% a b)
  (make-vec (+ (vec-x a)
               (vec-x b))
            (+ (vec-y a)
               (vec-y b))))

(define (v+ . vs)
  (fold v+% (make-vec 0 0) vs))



;; (v+ (v 1 2) (v 3 4))

;; (define inputs '(a b))
;; (define outputs '(c))

;; returns a list of three values.
;; Local coordinates for all input ports
;;            "              output ports
;; a pretty picture
(define (gate-image inputs outputs string)
  (define input-positions
    (map
     (lambda (name i)
       (define x 0)
       (define y (floor (+ (/ (- gate-box-height gate-height) 2)
                           (* (1+  i) (/ gate-height (1+ (length inputs)))))))
       (list (v x y) (v (/ (- gate-box-width gate-width) 2) y)))
     inputs
     (iota (length inputs))))
  (define output-positions
    (map
     (lambda (name i)
       (define x gate-box-width)
       (define y (floor (+ (/ (- gate-box-height gate-height) 2)
                           (* (1+  i) (/ gate-height (1+ (length outputs)))))))
       (list (v x y) (v (+ gate-width (/ (- gate-box-width gate-width) 2)) y)))
     outputs
     (iota (length outputs))))

  (list
   (map car input-positions)
   (map car output-positions)
   (cc-superimpose
    (apply cc-superimpose
           (map (lambda (pos)
                  (let ((p0 (list-ref pos 0)) (p1 (list-ref pos 1)))
                    (line (floor (vec-x p0)) (floor (vec-y p0))
                          (floor (vec-x p1)) (floor (vec-y p1))
                          gate-box-width
                          gate-box-height #:color "black")))
                (append input-positions output-positions)))

    (fill (rectangle gate-width gate-height) "white")
    (let ((text-size (- (min gate-width gate-height) 10)))
      (text string text-size text-size)))))

(define (gate-image-inputs gate)
  (list-ref gate 0))

(define (gate-image-outputs gate)
  (list-ref gate 1))

(define (gate-image-picture gate)
  (list-ref gate 2))

(gate-image '(a b) '(c) "= 1")

(define gates
  `((,(v 50 50) ,(gate-image '(a b) '(c) "≥ 1"))
    (,(v 150 150) ,(gate-image '(a b) '(c) "≥ 1")))
  )

(define base (fold (lambda (item done) (pin-over done
                                            (vec-x (car item))
                                            (vec-y (car item))
                                            (gate-image-picture (cadr item))))
                   (filled-rectangle 300 300 #:color "white")
                   gates))



(define l
  (let ()
    (define out-gate (list-ref gates 0))
    (define out-pos (v+ (car out-gate)
                        (car (gate-image-outputs (cadr out-gate)))))
    (define in-gate (list-ref gates 1))
    (define in-pos (v+ (car in-gate)
                       (car (gate-image-inputs (cadr in-gate)))))

    (line
     (vec-x in-pos) (vec-y in-pos)
     (vec-x out-pos) (vec-y out-pos)
     300 300
     #:color "black" #:stroke-width 1)))

(cc-superimpose base l)

;; (draw-diagram '(not (and a b)))
;; (define (draw-diagram expr)
;;   (match expr
;;     (('not a)
;;      (gate '(a) '(b) "!")
;;      )
;;     )
;;   )
;; =>
;; (gate '(c) '(d) "!")
;; (gate '(a b) '(c) "&")



(define-record-type (gate make-gate% gate?)
  (fields inputs outputs implementation))

(define* (make-gate #:key inputs outputs implementation)
  (make-gate% inputs outputs implementation))


(define half-adder-gate
  (make-gate #:inputs '(a b)
             #:outputs '(c s)
             #:implementation
             '((c (and a b))
               (s (xor a b)))))

(use-modules (srfi srfi-26))

(define (flatten-multis gate-implentation)
  (concatenate
   (map (lambda (step)
          (define input (car step))
          (define operation (cdr step))
          (if (list? input)
              (map (cut cons <> operation)
                   input)
              ;; else symbol
              (list step)))
        gate-implementation)))

#;
(define (gate-eval gate)
  (let ((in (gate-inputs gate))
        (out (gate-outputs gate))
        (impl (gate-implementation gate)))
    (define ht (make-hash-table))
    (for-each (lambda (pair)
                (define name (car pair))
                (define impl (cadr pair))
                (hash-set! ht impl
                           (or (hash-ref ht imple)
                               (eval-implementation impl)))
                )
              (flatten-multis impl))
    
    )
  )


#;
(concatenate
 (map (lambda (step)
        (define input (car step))
        (define operation (cdr step))
        (if (list? input)
            (map (cut cons <> operation)
                 input)
            ;; else symbol
            (list step)))
      (gate-implementation full-adder-gate)))

(define full-adder-gate
  (make-gate #:inputs '(a b c)
             #:outputs '(c s)
             #:implementation
             '((c (or c1 c0))
               (s s1)
               ((c1 s1) (half-adder s0 c))
               ((c0 s0) (half-adder a b)))))

(cc-superimpose base l)
